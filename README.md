#Запуск Docker + Laravel -> Sail

- Запустить  
`./vendor/bin/sail up`


- Запустить в фоне  
`sail up -d`


- Создать alias  
`alias alias sail='bash vendor/bin/sail'`


- Остановить  
`sail down`


- Узнать версию PHP приложения

./vendor/bin/sail php --version  
`sail php --version`


Composer
- Подключить Jetstream  

`./vendor/bin/sail composer require laravel/jetstream`  
`sail composer require laravel/jetstream`  


- Установить Jetstream с Inertia.js  

`./vendor/bin/sail artisan jetstream:install inertia`  
`sail artisan jetstream:install inertia` 


- Node.js/NPM  

`./vendor/bin/sail node --version`  
`sail node --version`  

`./vendor/bin/sail npm --version`  
`sail npm --version`  


- Установить зависимости  

`./vendor/bin/sail npm install`  
`sail npm install`  


- Скомпилировать js  

`./vendor/bin/sail npm run dev`  
`sail npm run dev`  


- Следить и компилировать  
`./vendor/bin/sail npm run watch`  
`sail npm run watch`  


- Development  
`./vendor/bin/sail npm run dev`  
`sail npm run dev`  


Artisan  
- Выполнить миграции  

`./vendor/bin/sail artisan migrate`  
`sail artisan migrate`  


- Просмотреть пути роутов  

`./vendor/bin/sail artisan route:list`  
`sail artisan route:list`  


Просмотр Email писем доступен по умолчанию  
`http://localhost:8025`  
(Использован пакет MailHog)


- Laravel Tinker  

`./vendor/bin/sail tinker`  
`sail tinker`  


- Share project  

`./vendor/bin/sail share`  
`sail share`  


- Указать поддомен сайта  

`./vendor/bin/sail share --subdomain=my-sail-site`  
`sail share --subdomain=my-sail-site`